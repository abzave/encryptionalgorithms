package server;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import server.Message;

public class MainWindowServer extends JFrame implements ActionListener {
	private ServerService server;
	
	public MainWindowServer() {
		this("No Title");
	}
	
	public MainWindowServer(String pTitle) {
		super(pTitle);
		
		this.setLayout(null);
		this.setSize(300, 400);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setExtendedState(Frame.NORMAL);
        
        initComponents();
        
        this.setVisible(true);
	}
		
	private void initComponents() {
		JButton iniciar = new JButton("Iniciar");
		iniciar.setBounds(20, 20, 100, 50);
		iniciar.addActionListener(this);
		iniciar.setName("iniciar");
		this.add(iniciar);
		
		JButton next = new JButton("Next Round");
		next.setBounds(20, 100, 100+40, 50);
		next.addActionListener(this);
		next.setName("next");
		this.add(next);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		JButton boton = (JButton)evt.getSource();
		
		if (boton.getName().compareTo("iniciar")==0) {
			server = new ServerService();
		}
		
		if (boton.getName().compareTo("next")==0) {
			for(String teamname: server.getTeamNames()) 
			{
				System.out.println("Enviando request de reto a team "+teamname);
				server.sendToOneRandom(teamname, new Message(0));
			}
		}		
	}
}
