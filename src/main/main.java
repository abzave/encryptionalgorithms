package main;

import gui.MainWindow;
import lib.IConstant;
import server.MainWindowServer;
import server.Message;
import server.SocketClient;

import javax.swing.JOptionPane;

public class main {

    public static void main(String[] args){
        MainWindowServer window = new MainWindowServer("Servidor de juego");
        String ip = JOptionPane.showInputDialog(IConstant.IP_MESSAGE);
        SocketClient socket = new SocketClient(ip);
        String team = JOptionPane.showInputDialog(IConstant.TEAM_MESSAGE);
        Message msg = new Message(IConstant.TEAM_MESSAGE_TYPE);
        msg.addField(IConstant.TEAM_NAME_REGISTRATION, team);
        msg.addField("nickname", "Abraham");
        socket.sendMsg(msg);
        new MainWindow("Juego PGP", 400, 300, socket);
        //---------------------------
        String ip2 = JOptionPane.showInputDialog(IConstant.IP_MESSAGE);
        SocketClient socket2 = new SocketClient(ip2);
        String team2 = JOptionPane.showInputDialog(IConstant.TEAM_MESSAGE);
        Message msg2 = new Message(IConstant.TEAM_MESSAGE_TYPE);
        msg2.addField(IConstant.TEAM_NAME_REGISTRATION, team2);
        msg2.addField("nickname", "Abraham");
        socket2.sendMsg(msg2);
        new MainWindow("Juego PGP", 400, 300, socket2);
    }

}
