package lib;

import java.math.BigInteger;

public interface IConstant{

    int RSA_BIT_LENGTH = 1024;
    int TRIPLE_DES_BYTE_LENGHT = 16;
    int DES_BYTE_LENGTH = 8;
    String IP_MESSAGE = "ip";
    String TEAM_MESSAGE = "Equipo";
    String TEAM_NAME_REGISTRATION = "teamname";
    int TEAM_MESSAGE_TYPE = 3;
    int ROWS = 3;
    int COLUMNS = 3;
    int BUTTON_WIDTH = 50;
    int BUTTON_HEIGHT = 50;
    int INITIAL_X = 75;
    int INITIAL_Y = 0;
    String[][] BUTTON_VALUES = {{"7Y", "45", "9F"}, {"R3", "BC", "Y1"}, {"63", "H5", "D3"}};
    int BUTTON_SEND_X = 250;
    int BUTTON_SEND_Y = 225;
    int BUTTON_SEND_HEIGHT = 25;
    int BUTTON_SEND_WIDTH = 100;
    int BUTTON_CHECK_X = 25;
    int BUTTON_CHECK_Y = 225;
    int BUTTON_CHECK_HEIGHT = 25;
    int BUTTON_CHECK_WIDTH = 100;
    String BUTTON_SEND_TEXT = "Enviar";
    String BUTTON_CHECK_TEXT = "Probar";

}
