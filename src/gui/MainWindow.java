package gui;

import encryptation.RSA;
import encryptation.TripleDES;
import lib.IConstant;
import server.Message;
import server.SocketClient;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Base64;
import java.util.Observable;
import java.util.Observer;

public class MainWindow extends JFrame implements Observer {

    private SocketClient socket;
    private String key = "";
    private String sendText = "";
    private String challengeString;
    private String challengeKey;
    private String decryptedMessage = "";
    private boolean selected = false;

    public MainWindow(String pTittle, int pWidth, int pHeight, SocketClient pSocket){
        socket = pSocket;
        socket.addObserver(this::update);
        setTitle(pTittle);
        setSize(pWidth, pHeight);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        initComponents();
    }

    private void initComponents(){
        setLayout(null);
        createMatrix();
        createButtons();
        repaint();
    }

    private void createMatrix(){
        int x;
        int y = IConstant.INITIAL_Y;
        for(int row = 0; row < IConstant.ROWS; row++){
            x = IConstant.INITIAL_X;
            y += IConstant.BUTTON_HEIGHT;
            for(int column = 0; column < IConstant.COLUMNS; column++){
                x += IConstant.BUTTON_WIDTH;
                JButton button = new JButton(IConstant.BUTTON_VALUES[row][column]);
                button.setBounds(x, y, IConstant.BUTTON_WIDTH, IConstant.BUTTON_HEIGHT);
                button.addActionListener(e -> this.keyAppend(button.getText()));
                add(button);
            }
        }
    }

    private void createButtons(){
        int x = IConstant.BUTTON_SEND_X;
        int y = IConstant.BUTTON_SEND_Y;
        int width = IConstant.BUTTON_SEND_WIDTH;
        int height = IConstant.BUTTON_SEND_HEIGHT;
        JButton sendButton = new JButton(IConstant.BUTTON_SEND_TEXT);
        JButton checkButton = new JButton(IConstant.BUTTON_CHECK_TEXT);
        sendButton.setBounds(x, y, width, height);
        x = IConstant.BUTTON_CHECK_X;
        y = IConstant.BUTTON_CHECK_Y;
        width = IConstant.BUTTON_CHECK_WIDTH;
        height = IConstant.BUTTON_CHECK_HEIGHT;
        checkButton.setBounds(x, y, width, height);
        sendButton.addActionListener(this::send);
        checkButton.addActionListener(this::check);
        add(sendButton);
        add(checkButton);
    }

    private void keyAppend(String keyName){
        key += keyName;
    }

    private void send(ActionEvent e){
        try {
            if(!selected) {
                Message newMessage = new Message(2);
                newMessage.addField("found_msg", decryptedMessage);
                socket.sendMsg(newMessage);
            }else {
                RSA.generateKeys();
                byte[] encryptedText = RSA.encrypt(sendText);
                String privateKey = Base64.getEncoder().encodeToString(RSA.getPrivateKey().getEncoded());
                String encryptedKey = TripleDES.encrypt(privateKey, key);
                Message newMessage = new Message(1);
                newMessage.addField("encrypted_msg", Base64.getEncoder().encodeToString(encryptedText));
                newMessage.addField("encrypted_priv", encryptedKey);
                socket.sendMsg(newMessage);
                selected = false;
            }
            key = "";
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void check(ActionEvent e){
        try {
            String privateKey = TripleDES.decrypt(challengeKey, key);
            decryptedMessage = RSA.decrypt(challengeString, privateKey);
            JOptionPane.showMessageDialog(null, decryptedMessage);
            key = "";
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        Message message = (Message) arg;
        if(message.getType() == 0){
            sendText = JOptionPane.showInputDialog("Ingrese la palabra clave");
            selected = true;
        }else if(message.getType() == 1){
            challengeString = message.getValue("encrypted_msg");
            challengeKey = message.getValue("encrypted_priv");
            JOptionPane.showMessageDialog(null, "New challenge!");
        }
    }

}
